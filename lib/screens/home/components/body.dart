import 'package:flutter/material.dart';
import 'package:plant_app/components.dart';

import 'feature_plants.dart';
import 'header_with_searchbox.dart';
import 'recommand_plants.dart';
import 'title_with_more_btn.dart';

class Body extends StatelessWidget {
  const Body({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          HeaderWtihSearchBox(size: size),
          TitleWithMoreBtn(title: "Recommanded", press: () {}),
          RecommandPlants(),
          TitleWithMoreBtn(title: "Feature Plants", press: () {}),
          FeaturePlants(),
          SizedBox(height: kDefaultpadding), // ?
        ],
      ),
    );
  }
}
